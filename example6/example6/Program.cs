﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace example6
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                ShowMessage("메시지를 출력");
                ShowMessage(null);
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("{0}", ex.Message);
            }
        }

        static void ShowMessage(string message)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            Console.WriteLine(message);
        }


        
        }
    }

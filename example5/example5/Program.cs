﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace example5
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = 0;
            for (int i = 1; i <= 10; i++)
            {
                result += i;
            }
            Console.WriteLine("for 문 : {0}", result);
            int startNumber = 1;
            int maxValue = 10;

            while (startNumber <= maxValue)
            {
                result += startNumber;
                startNumber++;
            }
            Console.WriteLine("{0}", result);
        
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;


namespace example8
{
    class Program
    {
        static void Main(string[] args)
        {
            string text1 = "";
            Stopwatch sw1 = Stopwatch.StartNew();
            for (int i = 0; i < 50000; i++)
            {
                text1 += i.ToString();
            }
            sw1.Stop();

            StringBuilder text2 = new StringBuilder();

            Stopwatch sw2 = Stopwatch.StartNew();
            for (int i = 0; i < 50000; i++)
            {
                text2.Append(i.ToString());

            }
            sw2.Stop();

            Console.WriteLine("String :{0}", sw1.ElapsedMilliseconds);
            Console.WriteLine("StringBuilder :{0}", sw2.ElapsedMilliseconds);

        }
    }
}
